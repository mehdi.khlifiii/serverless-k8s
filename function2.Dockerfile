FROM python:3.6.7

RUN mkdir -p /usr/src
WORKDIR /usr/src

COPY . /usr/src
COPY function_2/requirements.txt /usr/src

RUN apt-get update
RUN apt-get install python3-pip -y
RUN pip install -r requirements.txt

ENV PYTHONPATH=/usr/src

EXPOSE 8888

CMD ["python", "launcher.py"]
