import sys

from function_2.server import main

if __name__ == '__main__':
    try:
        port = sys.argv[1]
    except Exception as e:
        port = 8888
    main(port)


