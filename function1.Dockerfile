FROM python:3.7-slim

ENV APP_HOME /function1
WORKDIR $APP_HOME

COPY function_1 ./

RUN pip install FLASK gunicorn

CMD exec gunicorn --bind :$PORT --workers 1 --threads 8 --timeout 0 app:app