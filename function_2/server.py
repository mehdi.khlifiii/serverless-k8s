from mongoengine import connect
from function_2 import settings

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.web import Application
from function_2.settings import MONGO_DB_NAME, MONGO_KW
from function_2.views.function import FunctionView

entry_points = [
    (r"/second_function(.*)", FunctionView),
]


def main(port):
    app = Application(
        entry_points,
        debug=False
    )

    connect(
        MONGO_DB_NAME, **MONGO_KW
    )

    settings.LOG.info(
        "Tornado server has been started on port {}".format(port)
    )

    try:
        server = HTTPServer(app)
        server.bind(port)
        server.start(0)
        IOLoop.instance().start()
    except KeyboardInterrupt:
        settings.LOG.info("Exiting... Good Bye :D")
        IOLoop.instance().stop()


if __name__ == '__main__':
    main(8888)
