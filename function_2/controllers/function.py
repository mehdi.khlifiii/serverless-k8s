from function_2.controllers.base import ControllerBase
from function_2.models.function import FunctionModel


class FunctionController(ControllerBase):

    model = FunctionModel

