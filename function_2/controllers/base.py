import datetime

from function_2.utils.exceptions import (BadParametersError, ConflictError,
                                        ResourceNotFoundError)
from function_2.settings import LOG


class ControllerBase(object):

    model = None
    # we assume that for each model we have uuid
    # field as required and auto generated
    primary_keys = ["uuid"]

    @classmethod
    def list(cls, uuid=None):
        """
        get all data or get by uuid if "uuid" it is not None
        """
        if uuid is None or uuid == "" or uuid == "/":
            # get all data
            objects = cls.model.objects()
        else:
            LOG.info(uuid)
            objects = cls.model.objects.filter(
                uuid=uuid
            )
        if objects:
            return objects
        else:
            raise ResourceNotFoundError("Object not here ")

    @classmethod
    def check_if_exist(cls, **kwargs):
        """
        check primary keys if the are already exist
        in database and raise ConflictError
        """
        filter_by = {
            key: kwargs[key]
            for key in kwargs.keys()
            if key in cls.primary_keys
        }

        result = cls.model.objects.filter(
                **filter_by
            )

        if result:
            raise ConflictError(
                "Object already exist in database, primary kyes : {} {}".format(
                    cls.primary_keys, result
                )
            )

    @classmethod
    def pre_create(cls, **kwargs):
        """
        override this method if you want to put default values
        for some fields before the creation or to run some action.
        you can override this method with classic way or coroutine
        """
        return kwargs

    @classmethod
    def post_create(cls, **kwargs):
        """
        override this method if you want to
        run action after the creation in the database.
        you can override this method with classic way or coroutine
        """
        pass

    @classmethod
    def create(cls, **kwargs):
        """
        create new object in database
        """

        obj = cls.model(**kwargs)

        kwargs["uuid"] = str(obj.uuid)

        cls.pre_create(**kwargs)

        # checking the primary keys if they are exist
        # raise ConflictError if object is already exist
        cls.check_if_exist(**kwargs)

        obj.save()

        cls.post_create(**kwargs)

        return obj

    @classmethod
    def pre_update(cls, uuid, **kwargs):
        """
        override this method if you want to apply some actions
        before the update in the database
        you can override this method with classic way or coroutine
        """

        return kwargs

    @classmethod
    def post_update(cls, uuid, **kwargs):
        """
        override this method if you want to apply some actions
        after the update in the database
        you can override this method with classic way or coroutine
        """
        pass

    @classmethod
    def update(cls, uuid, **kwargs):
        """
        """

        objs = cls.model.objects.filter(uuid=uuid)

        if objs:
            obj = objs[0]
            cls.pre_update(uuid, **kwargs)

            for key, value in kwargs.items():
                obj.__setattr__(key, value)

            obj.modified_on = datetime.datetime.now()
            kwargs["uuid"] = str(obj.uuid)

            obj = obj.save()
            cls.post_update(**kwargs)

            return obj
        else:
            raise BadParametersError("Bad uuid")

    @classmethod
    def pre_delete(cls, uuid):
        """
        override this method if you want to apply some actions
        before the delete
        you can override this method with classic way or coroutine
        """

        pass

    @classmethod
    def post_delete(cls, uuid):
        """
        override this method if you want to apply some actions
        after the delete
        you can override this method with classic way or coroutine
        """
        pass

    @classmethod
    def delete(cls, uuid):
        """
        delete object from database by uuid
        """

        if uuid is None or uuid is "":
            raise BadParametersError("uuid is None")

        objs = cls.list(uuid)
        if len(objs) == []:
            raise ResourceNotFoundError(
                "No Resouce found with uuid {}".format(
                    uuid))
        obj = objs[0]

        cls.pre_delete(uuid)

        obj.delete()

        cls.post_delete(uuid)

        return obj

    @classmethod
    def search(cls, **kwargs):
        objects = cls.model.objects.filter(
                **kwargs
            )
        if objects:
            return objects
        else:
            raise ResourceNotFoundError("Object not found")
