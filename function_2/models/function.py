from function_2.models.base import BaseModel
from mongoengine import StringField, UUIDField
from uuid import uuid1


class FunctionModel(BaseModel):

    uuid = UUIDField(default=uuid1, required=True)
    name = StringField(required=False)

    def to_dict(self):
        """
        convert fields to dict
        """
        out = dict(
            uuid=str(self.uuid),
            name=self.name
        )
        return out
