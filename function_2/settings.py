import os
import configparser
from tornado.log import enable_pretty_logging, logging

import socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(('google.com', 0))
IP_ADDRESS = s.getsockname()[0]

LOG = logging.getLogger()
LOG.setLevel(logging.DEBUG)
enable_pretty_logging()

MONGO_DB_NAME = "serverless"
MONGO_KW = dict(
    host='mongodb://localhost:27017',
)
