from function_2.controllers.function import FunctionController
from function_2.views.base import ViewBase


class FunctionView(ViewBase):

    controller = FunctionController
