import os
from flask import Flask

app = Flask(__name__)


@app.route('/first_function')
def first_function():
    return 'Hello from Kubermachines'


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))
